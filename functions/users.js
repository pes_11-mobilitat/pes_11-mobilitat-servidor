// Import Admin SDK
var admin = require("firebase-admin");
admin.initializeApp();

// Get a database reference to our blog
var db = admin.database();
var ref = db.ref("users/");

module.exports.NewUser = function (jsonInfo) {
	var uid = jsonInfo["uid"];

	ref.child("users").orderByChild("uid").equalTo(uid).once("value", snapshot => {
		if (!snapshot.exists()) {
			var userRef = ref.child(uid);
			userRef.set(jsonInfo["profileInfo"]);
			userRef.update({
				downvotes: 0,
				upvotes: 0
			})
		}
	});
}

//jsonInfo contains the modified parameters
module.exports.EditUser = function (jsonInfo) {
	var uid = jsonInfo["uid"];

	var userRef = ref.child(uid);
	userRef.update(jsonInfo["profileInfo"]);
}

module.exports.DeleteUser = async function (uid) {
	await admin.auth().deleteUser(uid).then(user => {
		var userRef = ref.child(uid);
		userRef.set({
			name: "deleted user"
		}); //update overrides only the modified camps
		return;
	});
}

module.exports.LogoutUser = function (uid) {
	return admin.auth().revokeRefreshTokens(String(uid))
		.then(() => {
			return admin.auth().getUser(uid);
		})
		.then((userRecord) => {
			return new Date(userRecord.tokensValidAfterTime).getTime() / 1000;
		})
		.then((timestamp) => {
			console.log('Tokens revoked at: ', timestamp);
			return;
		});
}

module.exports.GetUser = async function (uid) {
	var data;
	data = await ref.child(uid).once("value", snapshot => {
		console.log(snapshot.val());
		ddata = snapshot.exportVal();
		return ddata;
	}, (errorObject) => {
		console.log("The read failed: " + errorObject.code);
	});
	return data;
}

module.exports.GetUserForOthers = async function (uid) {
	user = await ref.child(uid).once("value", snapshot => {
		return snapshot;
	}, function (errorObject) {
		console.log("The read failed: " + errorObject.code);
	});
	//falta debugar, pot ferse aixi ho modificant el snapshot directament.
	user = user.exportVal();
	user.password = "";
	user.device_token = "";
	return user;
}

module.exports.SetTokenDevice = function (jsonInfo) {
	var token = jsonInfo["device_token"];
	var uid = jsonInfo["uid"];
	if (token === undefined || uid === undefined) throw new String("You must specify uid and device token");
	ref.child(uid).update({ device_token: token });
}
