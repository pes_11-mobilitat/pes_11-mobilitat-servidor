var admin = require("firebase-admin");
var db = admin.database();
var uref = db.ref("users/");

module.exports.Verify = async function (req) {
    if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) {
        res = [403, "err"];
        return res
    }

    let checkRevoked = true;
    const idToken = req.headers.authorization.split('Bearer ')[1]
    user = await admin.auth().verifyIdToken(idToken, checkRevoked)
        .then(user => {
            console.log('valid token')
            res = [200, user.uid];
            return res;
        })
        .catch(error => {
            if (error.code === 'auth/id-token-revoked') {
                res = [401, error.code];
                console.log('token revoked')
            } else {
                console.log('3' + error)
                res = [403, error.code];
            }
        });
    return res;
}

module.exports.VerifyRole = async function (urole, uid) {
    let rrole;
    var userRef = uref.child(uid);

    await userRef.once("value", snapshot => {
        rrole = snapshot.val().rol;
        return;
    });

    console.log(rrole, urole, rrole === urole);

    return (rrole === urole);
}
