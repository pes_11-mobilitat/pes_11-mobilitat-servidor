// Import Admin SDK
var admin = require("firebase-admin");
// Get a database reference to our blog
var db = admin.database();
var ref = db.ref("help_requests/");
var ref_match = db.ref("match/");
var ref_users = db.ref("users/");
var ref_locations = db.ref("locations/");
var ref_chatroom = db.ref("chatroom/");

const valorations_functions = require('./valorations');

module.exports.NewHelpRequest = async function (jsonInfo) {
    var result = "OK";
    try {
        var uid = jsonInfo["uid"];
        var lat = jsonInfo["location_lat"];
        var lon = jsonInfo["location_lon"];
        var request_type = jsonInfo["request_type"];
        var estimated_duration = jsonInfo["estimated_duration"];
        await ref.once("value", async (snapshot) => {
            try {
                if (snapshot.exists()) {
                    var thenableReference = await ref.push({
                        created_at: new Date().getTime(),
                        accepted: false,
                        upm_id: uid,
                        location_lat: lat,
                        location_lon: lon,
                        urgent: true,
                        scheduled_at: null,
                        distance: 5,
                        request_type: request_type,
                        estimated_duration: estimated_duration
                    });
                    newRequestSendNotification(thenableReference.key, "Help", "I need Help!", "20");
                }
            } catch (e) {
                result = e.message;
            }
        });
    } catch (e) {
        result = e.message;
    }
    return result;
};


module.exports.GetNearRequest = async function (jsonInfo) {
    var lat = jsonInfo["location_lat"];
    var lon = jsonInfo["location_lon"];
    if (lat === undefined || lon === undefined) throw new String("You must specify location_lat and location_lon");
    request = await ref.once("value").then((snapshot) => {
        var selectedRequest = null;
        var selectedRequestRange = 20;
        snapshot.forEach((data) => {
            var accepted = data.val().accepted;
            var urgent = data.val().urgent;
            var request_lat = data.val().location_lat;
            var request_lon = data.val().location_lon;
            if (!accepted && urgent) {
                var distminima = data.val().distance;
                var range = calculatRange(lat, lon, request_lat, request_lon);
                if (range <= distminima && (selectedRequest === null || range < selectedRequestRange)) {
                    selectedRequest = data;
                    selectedRequestRange = range;
                }
            }
        });
        return selectedRequest;
    });
    var result = {};
    if (request === null) return result;
    else {
        result = request.exportVal();
        result["request_id"] = request.key;
        return result;
    }
};


module.exports.GetCurrentRequest = async function (uid) {
    var request;
    request = await ref.once("value").then((snapshot) => {
        var selectedRequest = null;
        snapshot.forEach((data) => {
            var data_uid = data.val().upm_id;
            var urgent = data.val().urgent;
            var time = data.val().created_at;
            var time_now = new Date().getTime();
            if (data_uid === uid && urgent && (calculateMinutes(time_now, time) < 60 * 24)) {
                selectedRequest = data;
            }
        });
        return selectedRequest;
    });
    var result = {};
    if (request === null) return result;
    else {
        result = request.exportVal();
        result["request_id"] = request.key;
        return result;
    }
};

module.exports.AcceptRequest = async function (jsonInfo) {
    var request_id = jsonInfo["request_id"];
    var uid = jsonInfo["uid"];
    var request = await ref.child(request_id).once("value", snapshot => {
        return snapshot;
    });
    if (!request.val().accepted) {
        ref.child(request_id).update({ accepted: true });
        var upm_id = request.val().upm_id;
        var request_type = request.val().request_type;
        var estimated_duration = request.val().estimated_duration;
        var chatroom_id;
        await ref_chatroom.once("value", snapshot => {
            if (snapshot.exists()) {
                var thenableReference = ref_chatroom.push();
                chatroom_id = thenableReference.key;
                ref_chatroom.child(chatroom_id).push({
                    message: "Welcome!"
                });
            }
        });
        await ref_match.once("value", snapshot => {
            if (snapshot.exists()) {
                var thenableReference = ref_match.push({
                    upm_id: upm_id,
                    uid: uid,
                    request_id: request_id,
                    chatroom_id: chatroom_id,
                    request_type: request_type,
                    estimated_duration: estimated_duration,
                    created_at: new Date().getTime(),
                    status: "ongoing"
                });
                acceptSendNotification(request_id, "Request Accepted", "I accept your request", "30");
            }
        });
    }
    else throw new Object({ status: 400, message: "Already accepted" });

};

async function newRequestSendNotification(request_id, title, body, codi) {
    var request = await ref.child(request_id).once("value", snapshot => {
        return snapshot;
    });
    var request_lon = request.val().location_lon;
    var request_lat = request.val().location_lat;
    var distance = request.val().distance;

    var toDoForEachLocation = async function (data) {
        var uid_lon = data.val().longitude;
        var uid_lat = data.val().latitude;
        if (calculatRange(request_lat, request_lon, uid_lat, uid_lon) <= distance) {
            var user = await ref_users.child(data.key).once("value", snapshot => {
                return snapshot;
            });
            var rol = user.val().rol;
            if (rol === "Ajudant") sendNotificationLocale(user.val().device_token, title, body, codi);
        }
    };

    await ref_locations.once("value", snapshot => {
        snapshot.forEach((data) => {
            toDoForEachLocation(data);
        });
    });
}

async function acceptSendNotification(request_id, title, body, codi) {
    var request = await ref.child(request_id).once("value", snapshot => {
        return snapshot;
    });
    var upm_id = request.val().upm_id;
    var upm = await ref_users.child(upm_id).once("value", snapshot => {
        return snapshot;
    });
    sendNotificationLocale(upm.val().device_token, title, body, codi);
}

function sendNotificationWithValoracio(token, title, body, codi, valoracio) {
    var message = {
        token: token,
        notification: {
            title: title,
            body: body
        },
        data: {
            codi: codi,
            valoracio: valoracio
        }
    };
    admin.messaging().send(message)
        .then((response) => {
            console.log('Successfully sent message:', response);
            return;
        })
        .catch((error) => {
            console.log('Error sending message:', error);
        });
}

function sendNotificationLocale(token, title, body, codi) {
    var message = {
        token: token,
        notification: {
            title: title,
            body: body
        },
        data: {
            codi: codi
        }
    };
    admin.messaging().send(message)
        .then((response) => {
            console.log('Successfully sent message:', response);
            return;
        })
        .catch((error) => {
            console.log('Error sending message:', error);
        });
}

module.exports.sendNotification = function (token, title, body, codi) {
    var message = {
        token: token,
        notification: {
            title: title,
            body: body
        },
        data: {
            codi: codi
        }
    };
    admin.messaging().send(message)
        .then((response) => {
            console.log('Successfully sent message:', response);
            return;
        })
        .catch((error) => {
            console.log('Error sending message:', error);
        });
}

//Update distance
module.exports.UpdateDistance = async function () {
    request = await ref.once("value").then((snapshot) => {
        snapshot.forEach((data) => {
            var request_time = data.val().created_at;
            var distance = data.val().distance;
            var actual_time = new Date().getTime();
            var accepted = data.val().accepted;
            if (distance < 20 && (calculateMinutes(actual_time, request_time) >= 5) && !accepted) {
                distance = distance + 5;
                ref.child(data.key).update({ distance: distance });
                newRequestSendNotification(data.key, "Help", "I need Help!", "20");
            }
        });
        return;
    });
};

//Finalize Match
module.exports.FinalizeMatch = async function (jsonInfo) {
    var match_id = jsonInfo["match_id"];
    var valoracio = jsonInfo["valoracio"];
    ref_match.child(match_id).update({ status: "finalized" });
    var match = await ref_match.child(match_id).once("value", snapshot => {
        return snapshot;
    });
    var voluntari_id = match.val().uid;
    var voluntari = await ref_users.child(voluntari_id).once("value", snapshot => {
        return snapshot;
    });
    await valorations_functions.ValorarMatch(valoracio, match_id);
    sendNotificationWithValoracio(voluntari.val().device_token, "Match finalitzat", "El UPM ha finalitzat el match", "50", valoracio)
};

//Time Expired Match
module.exports.MatchTimeExpired = async function () {
    request = await ref_match.once("value").then((snapshot) => {
        snapshot.forEach((data) => {
            var status = data.val().status;
            var actual_time = new Date().getTime();
            var estimated_duration = data.val().estimated_duration;
            var match_created_time = data.val().created_at;
            if (status === "ongoing" && (calculateMinutes(actual_time, match_created_time) >= (estimated_duration + 240))) {
                sendNotificationLocale(data.val().upm_id, "Match Time Expired", "Match Time Expired", "40");
                sendNotificationLocale(data.val().uid, "Match Time Expired", "Match Time Expired", "40");
                ref_match.child(data.key).update({ status: "finalized" });
            }
        });
        return;
    });
};

function calculateMinutes(timestamp1, timestamp2) {
    var difference = timestamp1 - timestamp2;
    var daysDifference = Math.floor(difference / 1000 / 60 / 60);

    return daysDifference;
}


function calculatRange(lat1, lon1, lat2, lon2) {
    if ((lat1 === lat2) && (lon1 === lon2)) {
        return 0;
    }
    else {
        var radlat1 = Math.PI * lat1 / 180;
        var radlat2 = Math.PI * lat2 / 180;
        var theta = lon1 - lon2;
        var radtheta = Math.PI * theta / 180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;
        return dist;
    }
}
