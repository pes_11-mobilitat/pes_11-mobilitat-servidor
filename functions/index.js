/* eslint-disable promise/no-nesting */
const functions = require('firebase-functions');
const user_functions = require('./users');
const request_functions = require('./help_requests');
const location_functions = require('./location');
const verify = require('./verifier');
const messaging_functions = require('./messaging');
const event_functions = require('./calendar');
const hr_functions = require('./help_requests');

var admin = require("firebase-admin");

var db = admin.database();
var ref_users = db.ref("users/");

exports.NewHelpRequest = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	console.log(jsonInfo.toString());
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		console.log(uid + " " + String(res[1]) + " " + (parseInt(res[0])));
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			request_functions.NewHelpRequest(jsonInfo).then(result => {
				if (result === "OK") {
					response.status(200).send('OK');
				} else {
					response.status(500).send(result);
				}
				return;
			}).catch(error => {
				response.status(500).send(error);
				return;
			});
		} else {
			response.status(403).send('Unauthorized')
		}
		return
	}).catch(error => { console.log(error) });
});

exports.GetCurrentRequest = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	console.log(jsonInfo.toString());
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		console.log(uid + " " + String(res[1]) + " " + (parseInt(res[0])));
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			request_functions.GetCurrentRequest(uid).then(result => {
				response.status(200).send(result);
				return;
			}).catch(error => {
				response.status(500).send(error);
				return;
			});
		} else {
			response.status(403).send('Unauthorized')
		}
		return
	}).catch(error => { console.log(error) });
});

exports.GetNearRequest = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	console.log(jsonInfo.toString());
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		console.log(uid + " " + String(res[1]) + " " + (parseInt(res[0])));
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			request_functions.GetNearRequest(jsonInfo).then(data => {
				response.status(200).send(data);
				return;
			}).catch(error => {
				response.status(500).send(error);
				return;
			});
		} else {
			response.status(403).send('Unauthorized')
		}
		return
	}).catch(error => { console.log(error) });
});

exports.AcceptRequest = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	console.log(jsonInfo.toString());
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		console.log(uid + " " + String(res[1]) + " " + (parseInt(res[0])));
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			request_functions.AcceptRequest(jsonInfo).then(data => {
				response.status(200).send('OK');
				return;
			}).catch(error => {
				response.status(error.status).send(error.message)
			});
		} else {
			response.status(403).send('Unauthorized')
		}
		return
	}).catch(error => { console.log(error) });
});

exports.FinalizeMatch = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	console.log(jsonInfo.toString());
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		console.log(uid + " " + String(res[1]) + " " + (parseInt(res[0])));
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			request_functions.FinalizeMatch(jsonInfo).then(data => {
				response.status(200).send('OK');
				return;
			}).catch(error => {
				response.status(error.status).send(error.message)
			});
		} else {
			response.status(403).send('Unauthorized')
		}
		return
	}).catch(error => { console.log(error) });
});

exports.MatchTimeExpired = functions.https.onRequest((request, response) => {
	request_functions.MatchTimeExpired();
});

exports.UpdateDistance = functions.https.onRequest((request, response) => {
	request_functions.UpdateDistance();
});

exports.SetTokenDevice = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	console.log(jsonInfo.toString());
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		try {
			console.log(uid + " " + String(res[1]) + " " + (parseInt(res[0])));
			if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
				user_functions.SetTokenDevice(jsonInfo);
				response.status(200).send('OK');
			} else {
				response.status(403).send('Unauthorized')
			}
		} catch (error) {
			response.status(500).send(error)
		}
		return
	}).catch(error => { console.log(error) });
});

exports.NewUser = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	console.log(jsonInfo.toString());
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		console.log(uid + " " + String(res[1]) + " " + (parseInt(res[0])));
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			user_functions.NewUser(jsonInfo);
			response.status(200).send('OK');
		} else {
			response.status(403).send('Unauthorized')
		}
		return
	}).catch(error => { console.log(error) });
});

exports.GetUser = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		if ((parseInt(res[0]) === 200)) {
			if (uid === String(res[1])) {
				data = user_functions.GetUser(uid).then(data => {
					response.status(200).send(data);
					return;
				}).catch(error => {
					response.status(403).send(error)
					return;
				});
			}
			else {
				data = user_functions.GetUserForOthers(uid).then(data => {
					response.status(200).send(data);
					return;
				}).catch(error => {
					response.status(403).send(error)
					return;
				});
			}

		} else {
			response.status(res[0]).send(res[1])
		}
		return
	}).catch(error => { console.log(error) });
});


exports.EditUser = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			user_functions.EditUser(jsonInfo);
			response.status(200).send('OK');
		} else {
			response.status(res[0]).send(res[1])
		}
		return
	}).catch(error => { console.log(error) });
});

exports.LogoutUser = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			user_functions.LogoutUser(uid).catch(error => { response.status(403).send(error) });
			response.status(200).send('OK');
		} else {
			response.status(403).send('Unauthorized')
		}
		return
	}).catch(response.status(403).send(error));
});

exports.DeleteUser = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		console.log("uid " + uid);
		console.log('verify values: ' + res[0] + " " + res[1]);
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			user_functions.DeleteUser(uid).then(() => {
				response.status(200).send('OK');
				return;
			}).catch(error => {
				response.status(403).send(error)
				return;
			});

		} else {
			response.status(403).send('Unauthorized')
		}
		return;
	}).catch(error => { response.status(403).send(error) });
});

/*********** LOCATION ***********/

exports.UpdateLocation = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	var lat = jsonInfo["lat"];
	var lon = jsonInfo["lon"];

	verify.Verify(request).then(res => {
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			location_functions.UpdateLocation(uid, lat, lon);
			response.status(200).send('OK');
		} else {
			response.status(res[0]).send(res[1])
		}
		return
	}).catch(error => { console.log(error) });
});

exports.GetLocation = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		if (parseInt(res[0]) === 200) { // do better when integrated
			data = location_functions.GetLocation(uid).then(data => {
				response.status(200).send(data);
				return;
			}).catch(error => {
				response.status(403).send(error)
				return;
			});
		} else {
			response.status(res[0]).send(res[1])
		}
		return
	}).catch(error => { console.log(error) });
});


/*********************************************/
/***************** MESSAGING *****************/
/*********************************************/

exports.GetMatch = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			match = messaging_functions.GetCurrentMatch(uid).then(match => {
				response.status(200).send(match);
				return;
			}).catch(error => {
				console.log('1' + error);
				response.status(403).send(error);
				return;
			});
		} else {
			console.log('2' + res[1]);
			response.status(res[0]).send(res[1])
		}
		return
	}).catch(error => { console.log(error) });
});

exports.GetChatMessages = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	var chatID = jsonInfo["cid"];
	verify.Verify(request).then(res => {
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			msg = messaging_functions.GetMessages(chatID).then(msg => {
				response.status(200).send(msg);
				return;
			}).catch(error => {
				console.log('1' + error);
				response.status(403).send(error);
				return;
			});
		} else {
			console.log('2' + res[1]);
			response.status(res[0]).send(res[1])
		}
		return
	}).catch(error => { console.log(error) });
});

exports.SendMessage = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			sc = messaging_functions.SendMessage(jsonInfo);
			response.status(sc).send('OK');
		} else {
			console.log('2' + res[1]);
			response.status(res[0]).send(res[1])
		}
		return;
	}).catch(error => { console.log(error) });
});



exports.SendChatNotification = functions.database.ref('/chatroom/{cid}/{msgid}').onWrite((change, context) => {
	//const cid = event.params.cid;
	const cid = context.params.cid;
	const msgid = context.params.msgid;


	messaging_functions.GetMessage(cid, msgid).then(ddata => {
		var msgContent = ddata["message"] // message content to be sent.
		console.log(JSON.stringify(ddata) + " " + cid + " " + msgid); // data of the new message

		messaging_functions.GetCurrentMatch(ddata["sender"]).then(match => {
			var vol = match["uid"];
			var upm = match["upm_id"];

			// get receiver
			var receiverID = vol;
			if (vol === ddata["sender"]) {
				receiverID = upm;
			}

			messaging_functions.GetDeviceToken(receiverID).then(res => {
				hr_functions.sendNotification(res, 'Tens un missatge nou del teu Match!', msgContent, '60');
				return;
			}).catch(err => {
				console.log('2 ', err)
			});
			return;
		}).catch(error => {
			console.log('1 ' + error);
			return;
		});
		return;
	}).catch(error => {
		console.log('2 ' + error);
		return;
	});
});

/* EVENTS */
exports.NewEvent = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	console.log(jsonInfo.toString());
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			verify.VerifyRole('UPM', uid).then(res => {
				if (res === true) {
					event_functions.NewEvent(jsonInfo["eventinfo"]);
					response.status(200).send('OK');
					return;
				} else {
					response.status(403).send('Unauthorized');
					return;
				}
			}).catch(error => { console.log(error) });
		} else {
			response.status(403).send('Unauthorized')
			return;
		}
		return;
	}).catch(error => { console.log(error) });
});

exports.EditEvent = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	console.log(jsonInfo.toString());
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			verify.VerifyRole('UPM', uid).then(res => {
				if (res === true) {
					eventid = jsonInfo["eventid"]
					eventinfo = jsonInfo["eventinfo"]
					event_functions.EditEvent(eventid, eventinfo).catch(e => {
						console.log(e)
						response.status(500).send('Internal server error')
					});
					response.status(200).send('OK')
				} else {
					response.status(403).send('Unauthorized')
				}
				return;
			}).catch(error => { console.log(error) });
		} else {
			response.status(403).send('Unauthorized')
		}
		return
	}).catch(error => { console.log(error) });
});

exports.DeleteEvent = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	console.log(jsonInfo.toString());
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			verify.VerifyRole('UPM', uid).then(res => {
				if (res === true) {
					event_functions.DeleteEvent(jsonInfo["eventid"]).catch(e => {
						console.log(e)
						response.status(500).send('Internal server error')
						return;
					});
					response.status(200).send('OK')
				} else {
					response.status(403).send('Unauthorized')
				}
				return;
			}).catch(error => { console.log(error) });
		} else {
			response.status(403).send('Unauthorized')
		}
		return
	}).catch(error => { console.log(error) });
});

exports.JoinEvent = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	console.log(jsonInfo.toString());
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			verify.VerifyRole('Ajudant', uid).then(res => {
				if (res === true) {
					event_functions.AttendEvent(uid, jsonInfo["eventid"]).catch(e => {
						console.log(e)
						response.status(500).send('Internal server error')
					});
					response.status(200).send('OK')
				} else {
					response.status(403).send('Unauthorized')
				}
				return;
			}).catch(error => { console.log(error) });
		} else {
			response.status(403).send('Unauthorized')
		}
		return;
	}).catch(error => { console.log(error) });
});

exports.LeaveEvent = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	console.log(jsonInfo.toString());
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			verify.VerifyRole('Ajudant', uid).then(res => {
				if (res === true) {
					console.log('debug');
					event_functions.LeaveEvent(jsonInfo["eventid"]).catch(e => {
						console.log(e)
						response.status(500).send('Internal server error')
					});
					response.status(200).send('OK')
				} else {
					console.log('debug2');
					response.status(403).send('Unauthorized')
				}
				return;
			}).catch(error => { console.log(error) });
		} else {
			response.status(403).send('Unauthorized')
		}
		return;
	}).catch(error => { console.log(error) });
});

exports.GetEventPublic = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	jsonInfo = JSON.parse(jsonInfo);
	var eventid = jsonInfo["eventid"];
	event_functions.GetEvent(true, eventid).then(res => {
		response.status(200).send(res);
		return;
	}).catch(error => {
		console.log(error);
		response.status(500).send('Internal server error');
	});
});

exports.GetEventPrivate = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	console.log(jsonInfo.toString());
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	var eventid = jsonInfo["eventid"];
	verify.Verify(request).then(res => {
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			event_functions.GetEvent(false, eventid).then(res => {
				console.log(res);
				response.status(200).send(res);
				return;
			}).catch(error => {
				console.log(error);
				response.status(500).send('Internal server error');
			});
		} else {
			response.status(403).send('Unauthorized')
		}
		return;
	}).catch(error => { console.log(error) });
});

exports.GetEventsUPM = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	console.log(jsonInfo.toString());
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			verify.VerifyRole('UPM', uid).then(res => {
				if (res === true) {
					events = event_functions.GetEventsUPM(uid);
					response.status(200).send(events)
				} else {
					console.log('debug2');
					response.status(403).send('Unauthorized')
				}
				return;
			}).catch(error => { console.log(error) });
		} else {
			response.status(403).send('Unauthorized')
		}
		return;
	}).catch(error => { console.log(error) });
});

exports.GetEventsVolunteer = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString();
	console.log(jsonInfo.toString());
	jsonInfo = JSON.parse(jsonInfo);
	var uid = jsonInfo["uid"];
	verify.Verify(request).then(res => {
		if ((parseInt(res[0]) === 200) && (uid === String(res[1]))) {
			verify.VerifyRole('Ajudant', uid).then(res => {
				if (res === true) {
					events = event_functions.GetEventsVolunteer(uid);
					response.status(200).send(events);
				} else {
					console.log('debug2');
					response.status(403).send('Unauthorized')
				}
				return;
			}).catch(error => { console.log(error) });
		} else {
			response.status(403).send('Unauthorized')
		}
		return;
	}).catch(error => { console.log(error) });
});

exports.GetPublicEventsFiltered = functions.https.onRequest((request, response) => {
	var jsonInfo = request.body.toString(); //filters
	jsonInfo = JSON.parse(jsonInfo);
	var e = event_functions.GetListWithFilters(jsonInfo);
	response.status(200).send(e);
	//this will prob be async
});