// Import Admin SDK
var admin = require("firebase-admin");

// Get a database reference to our blog
var db = admin.database();
var chat = db.ref("chatroom/");
var match = db.ref("match/");
var ref_users = db.ref("users/")

module.exports.GetMessages = async function (chatID) {
    messages = await chat.child(chatID).once("value", snapshot => {
        console.log(snapshot.val());
        ddata = snapshot.exportVal();
        return ddata;
    }, (errorObject) => {
        console.log("The read failed: " + errorObject.code);
    });
    return messages;
}

module.exports.GetCurrentMatch = async function (userID) {
    let data = null;
    matchID = await match.orderByChild('status').equalTo('ongoing').once("value", snapshot => {
        snapshot.forEach((child) => {
            volID = child.child("uid").val();
            upmID = child.child("upm_id").val();
            if (volID === userID || upmID === userID) {
                data = child.val();
                data["match_id"] = child.key;
                return data;
            }
            return null;
        });
    }, (errorObject) => {
        console.log("The read failed: " + errorObject.code);
    });
    console.log(data)
    return data;
}

// user sends a message and it is stored in the database
/* messageData:
{
    "cid":"whatever",
    "uid":"whatever",
    "msg":"whatever"
}
*/
module.exports.SendMessage = function (messageData) {
    var chatID = messageData["cid"];
    var userID = messageData["uid"];
    var msgContent = messageData["msg"];
    var msgTime = admin.database.ServerValue.TIMESTAMP;
    var statusCode = 403;

    if (verifyChatMember(chatID, userID)) {
        chat.child(chatID).once("value", snapshot => {
            if (snapshot.exists()) {
                chat.child(chatID).push({
                    message: msgContent,
                    messagetime: msgTime,
                    sender: userID
                });
            }
        });
        statusCode = 200;
    }
    return statusCode
}

// returns boolean
verifyChatMember = async function (chatID, memberID) {
    valid = false;
    matchID = await match.orderByChild('chatroomID').equalTo(chatID).once("value", snapshot => {
        snapshot.forEach((child) => {
            volID = child.child("VolunteerId").val();
            upmID = child.child("UPMId").val();
            cid = child.child("chatroomID").val();
            valid = (volID === memberID || upmID === memberID) && chatID === cid;
        });
    }, (errorObject) => {
        console.log("The read failed: " + errorObject.code);
    });
    return valid;
}

module.exports.GetMessage = async function (chatid, msgid) {
    message = await chat.child(chatid).child(msgid).once("value", snapshot => {
        return snapshot;
    }, (errorObject) => {
        console.log("The read failed: " + errorObject.code);
    });
    console.log('message', message.val())
    return message.val();
}

module.exports.GetDeviceToken = async function (uid) {
    var snap;
    await ref_users.child(uid).once("value", snapshot => {
        snap = snapshot.val().device_token;
        return snapshot;
    });
    console.log('device token', snap);
    return snap;
}