// Import Admin SDK
var admin = require("firebase-admin");

// Get a database reference to our blog
var db = admin.database();
var ref = db.ref("locations/");

module.exports.UpdateLocation = function (uid, lat, lon) {
	var locRef = ref.child(uid);
	locRef.set({
		latitude: lat,
		longitude: lon,
	});
}

module.exports.GetLocation = async function (uid) {
	var data;
	data = await ref.child(uid).once("value", snapshot => {
		console.log(snapshot.val());
		ddata = snapshot.exportVal();
		return ddata;
	}, (errorObject) => {
		console.log("The read failed: " + errorObject.code);
	});
	return data;
}