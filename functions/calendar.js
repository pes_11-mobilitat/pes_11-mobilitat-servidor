// Import Admin SDK
var admin = require("firebase-admin");
// Get a database reference to our blog
var db = admin.database();
var ref = db.ref("calendar/");
var cref = db.ref("chatroom/");
const messaging_functions = require('./messaging');
const hr_functions = require('./help_requests');

module.exports.NewEvent = function (eventInfo) {
    var newPostRef = ref.push();
    newPostRef.set(eventInfo);
}

module.exports.EditEvent = async function (eventid, eventInfo) {
    // Edit event
    var ref2 = ref.child(eventid);
    ref2.update(eventInfo);

    // Send notification to assigned volunteer?
    await ref.child(eventid).once("value", snapshot => {
        if (snapshot.child("volunteer").exists()) {
            vid = snapshot.val().volunteer;
            this.sendNoti(vid, "Un event en el que participaves ha estat editat", 100)
        }
        return snapshot;
    });
}

module.exports.DeleteEvent = async function (eventid) {
    // get volunteer_id if any
    let vid;

    // notify that the event will be erased
    await ref.child(eventid).once("value", snapshot => {
        if (snapshot.child("volunteer").exists()) {
            vid = snapshot.val().volunteer;
            this.sendNoti(vid, "Un event en el que participaves ha estat esborrat", 110)
        }
        return snapshot;
    });

    // delete event
    var ref2 = ref.child(eventid);
    ref2.set(null);
}

module.exports.AttendEvent = async function (uid, eventid) {

    // check that there is no assigned volunteer
    await ref.child(eventid).once("value", snapshot => {
        console.log(snapshot.val());
        if (!snapshot.child("volunteer").exists()) {
            cid = createChat()
            ref.child(eventid).update({
                volunteer: uid,
                cid: cid,
            });
            upmid = snapshot.val().uid;
            this.sendNoti(upmid, "S'ha trobat un voluntari per un dels teus events", 80);
        }
        return snapshot;
    });
}

module.exports.LeaveEvent = async function (eventid) {
    // set camp to empty or delete it 
    var ref2 = ref.child(eventid);

    // delete chatroom (just deleting the chat reference, the chat itself is saved)
    ref2.update({ volunteer: null, cid: null })
    console.log('debug3');
    // Send notification to UPM
    await ref.child(eventid).once("value", snapshot => {
        uid = snapshot.val().uid;
        this.sendNoti(uid, "El voluntari a un dels teus events ha cancel·lat la seva ajuda", 90);
        return snapshot;
    });
}

module.exports.sendNoti = function (uid, msg, code) {
    console.log('sending notification to', uid);
    messaging_functions.GetDeviceToken(receiverID).then(res => {
        hr_functions.sendNotification(res, 'Tens un missatge nou del teu Match!', msgContent, '60');
        return;
    }).catch(err => {
        console.log('2 ', err)
    });
}

module.exports.GetEvent = async function (pub, eventid) {
    event = ref.child(eventid);

    let ev;

    await event.once("value", snapshot => {
        ev = snapshot.val();
        console.log(ev);
        return snapshot;
    });

    // by definition, an event is public when it is looking for volunteers
    if (pub === true && !object.hasOwnProperty('volunteer')) {
        return ev;
    } else if (pub === false) {
        return ev;
    } else {
        return null;
    }
}

module.exports.GetEventsUPM = function (uid) {
    let events = [];

    ref.orderByChild('uid').equalTo(uid).on("value", (snapshot) => {
        snapshot.forEach((data) => {
            //ADD DATA TO AN OBJECT AND PUSH IT TO THE EVENTS ARRAY
            var id = data.key;
            var title = data.val().title;
            var date = data.val().date;
            var lat = data.val().lat;
            var lon = data.val().lon;
            var eventShort = { id: id, title: title, date: date, lat: lat, lon: lon };
            console.log('event short:', eventShort);
            events.push(eventShort);
        });
    });

    return events;
}

module.exports.GetEventsVolunteer = function (uid) {
    //same but instead of 'uid' we use 'volunteer'
    let events = [];

    ref.orderByChild('volunteer').equalTo(uid).on("value", (snapshot) => {
        snapshot.forEach((data) => {
            //ADD DATA TO AN OBJECT AND PUSH IT TO THE EVENTS ARRAY
            var id = data.key;
            var title = data.val().title;
            var date = data.val().date;
            var lat = data.val().lat;
            var lon = data.val().lon;
            var eventShort = { id: id, title: title, date: date, lat: lat, lon: lon };
            console.log('event short:', eventShort);
            events.push(eventShort);
        });
    });
    return events;
}

module.exports.GetListWithFilters = function (filters) {
    // Get all filters existence and values if any
    var filterData = filters.hasOwnProperty('date');
    var date;
    if (filterData === true) { //only written if it has the filter
        date = filters["date"];
    }

    var filterDaytime = filters.hasOwnProperty('daytime');
    var daytime;
    if (filterDaytime === true) {
        daytime = filters["daytime"];
    }

    var filterDistance = filters.hasOwnProperty('dist');
    var max_dist;
    if (filterDistance === true) {
        max_dist = filters["dist"];
    }

    // Get all public events
    // for each public event, add the item to the return array if it passes all the filters set to true
    let events = [];

    ref.on("value", (snapshot) => {
        date2 = date;
        daytime2 = daytime;
        max_dist2 = max_dist;
        snapshot.forEach((data) => {
            console.log(daytime, max_dist, date, filterData, filterDaytime, filterDistance);
            //ADD DATA TO AN OBJECT AND PUSH IT TO THE EVENTS ARRAY
            var addValue = true;

            if (data.hasChild('volunteer') || data.hasChild('cid')) {
                console.log("entering");
                addValue = false;
            }
            if (filterData === true) {
                if (data.val().date !== date) {
                    addValue = false;
                }
            }
            if (filterDaytime === true) {
                if (data.val().daytime !== daytime) {
                    addValue = false;
                }
            }
            if (filterDistance === true) {
                var radlat1 = Math.PI * data.val().lat / 180;
                var radlat2 = Math.PI * max_dist["lat"] / 180;
                var theta = data.val().lon - max_dist["lon"];
                var radtheta = Math.PI * theta / 180;
                var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                if (dist > 1) {
                    dist = 1;
                }
                dist = Math.acos(dist);
                dist = dist * 180 / Math.PI;
                dist = dist * 60 * 1.1515;
                dist = dist * 1.609344;

                if (dist > max_dist["radius"]) {
                    addValue = false;
                }
            }
            // if it passes all filters we add the value to the events
            if (addValue === true) {
                ev = data.val();
                ev["id"] = data.key
                events.push(ev);
                console.log(events); //No esborrar :)
            }
        });
    });
    console.log("events", events);
    return events;
}

createChat = function () {
    var chatref = cref.push();
    cid = chatref.key;
    chatref.push({
        message: "Welcome!"
    });
    return cid;
}