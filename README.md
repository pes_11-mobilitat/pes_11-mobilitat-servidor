# Backend server

## Crides pel xat
#### GetMessages
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/GetChatMessages
+ Method: POST
+ Content type: text/plain
+ Authorization: Bearer <ID_TOKEN>
+ Body: Un JSON amb el format següent

```
{
    "uid":"User_id",
    "cid":"Chat id from match id or somewhere else"
}
```

#### GetMatch
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/GetMatch
+ Method: POST
+ Content type: text/plain
+ Authorization: Bearer <ID_TOKEN>
+ Body: Un JSON amb el format següent

```
{
    "uid":"User_id"
}
```

Example return:
```
{
    "UPMId": "randomUPMID",
    "VolunteerId": "5saGRnkBW9f8NP28uU7JCmxrYbD2",
    "chatroomID": "randomvalue",
    "status": "ongoing",
    "request_type":"request_type",
    "estimated_duration":"estimated_duration",
    "created_at":"timestamp",
    "match_id":"match_id"
}
```

#### FinalizeMatch
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/FinalizeMatch
+ Method: POST
+ Content type: text/plain
+ Authorization: Bearer <ID_TOKEN>
+ Body: Un JSON amb el format següent

```
{
    "uid":"User_id".
    "match_id":"match_id"
}
```

#### SendMessage
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/SendMessage
+ Method: POST
+ Content type: text/plain
+ Authorization: Bearer <ID_TOKEN>
+ Body: Un JSON amb el format següent

```
{
    "uid":"User_id"
    "cid":"Chat_id"
    "msg":"your message"
}
```

## Gestionar usuaris
### Crides a firebase functions
#### NewUser
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/NewUser 
+ Method: POST
+ Content type: text/plain
+ Authorization: Bearer <ID_TOKEN>
+ Body: Un JSON amb el format següent

```
{
    "uid":"User_id",
    "profileInfo":{
        "role":"ROLE NAME IN UPPERCASE",
        "name":"Nom de la persona",
        "resta de la info":"..."
    }
}
```

#### EditUser
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/EditUser
+ Method: POST
+ Content type: text/plain (HEADER)
+ Authorization: Bearer <ID_TOKEN>
+ Body:

```
{
    "uid":"user_id",
    "profileInfo":{
        "role":"ROLE NAME IN UPPERCASE",
        "name":"Nom de la persona",
        "resta de la info":"..."
    }
}
```
 
#### DeleteUser
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/DeleteUser 
+ Method: POST
+ Content type: text/plain
+ Authorization: Bearer <ID_TOKEN>
+ Body: Un JSON amb el format següent
```
{
    "uid":"user_id"
}
```

#### GetUser
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/GetUser 
+ Method: POST
+ Content type: text/plain
+ Authorization: Bearer <ID_TOKEN>
+ Body: Un JSON amb el format següent
```
{
    "uid":"user_id"
}
```

### Crides al server de Auth
#### SignUp
+ URL: https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyCKS5NMnVoWUQFOGJstDE89uIgYf5p2qOc
+ Method: POST
+ Content-Type: application/json
+ + Body: Un JSON amb el format següent
```
{
    "email":"valid_email",
    "password":"whatever",
    "returnSecureToken":true
}
```

#### Login
+ URL: https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyCKS5NMnVoWUQFOGJstDE89uIgYf5p2qOc
+ Method: POST
+ Content-Type: application/json
+ Body: Un JSON amb el format següent
```
{
    "email":"valid_email",
    "password":"whatever",
    "returnSecureToken":true
}
```

#### Logout
+ URL:  https://us-central1-pes11-mobilitat.cloudfunctions.net/LogoutUser 
+ Method: POST
+ Content-Type: text/plain
+ Authorization: Bearer <ID_TOKEN>
+ Body: Un JSON amb el format següent:
```
{
    "uid":"user_id"
}
```

#### Refresh del Token
+ URL: https://securetoken.googleapis.com/v1/token?key=AIzaSyCKS5NMnVoWUQFOGJstDE89uIgYf5p2qOc
+ Method: POST
+ Content-Type: x-www-form-urlencoded
+ Body: Key-Values
```
grant_type      refresh_token
refresh_token   <ID_token>
```

#### SetTokenDevice
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/SetTokenDevice
+ Method: POST
+ Content type: text/plain (HEADER)
+ Authorization: Bearer <ID_TOKEN>
+ Body:

```
{
    "device_token":"device_token",
    "uid":"uid"
}
```
## Localització

#### Update localització
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/UpdateLocation
+ Method: POST
+ Content type: text/plain (HEADER)
+ Authorization: Bearer <ID_TOKEN>
+ Body:

```
{
    "uid":"user_id",
    "lat":"122.00",
    "lon":"33.00"
}
```

#### Get localització
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/GetLocation
+ Method: POST
+ Content type: text/plain (HEADER)
+ Authorization: Bearer <ID_TOKEN>
+ Body:

```
{
    "uid":"user_id de qui es vol obtenir la localització",
}
```

## Help Request

#### NewHelpRequest
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/NewHelpRequest
+ Method: POST
+ Content type: text/plain (HEADER)
+ Authorization: Bearer <ID_TOKEN>
+ Body:

```
{
    "uid":"uid",
    "request_type":"request_type",
    "estimated_duration":"estimated_duration",
    "location_lat":"location_lat",
    "location_lon":"location_lon"
}
```
#### GetCurrentRequest
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/GetCurrentRequest
+ Method: POST
+ Content type: text/plain (HEADER)
+ Authorization: Bearer <ID_TOKEN>
+ Body:

```
{
    "uid":"uid"
}
```
+ Returns a Request:
```
{
    "request_id":"request_id",
    "created_at":"timestamp",
    "accepted":"false",
    "upm_id":"upm_id",
    "location_lat":"location_lat",
    "location_lon":"location_lon",
    "urgent":"true",
    "distance":"distance",
    "request_type":"request_type",
    "estimated_duration":"estimated_duration"
}
```
#### GetNearRequest
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/GetNearRequest
+ Method: POST
+ Content type: text/plain (HEADER)
+ Authorization: Bearer <ID_TOKEN>
+ Body:

```
{
    "uid":"uid",
    "location_lat":"location_lat",
    "location_lon":"location_lon"
}
```
+ Returns a Request:
```
{
    "request_id":"request_id",
    "created_at":"timestamp",
    "accepted":"false",
    "upm_id":"upm_id",
    "location_lat":"location_lat",
    "location_lon":"location_lon",
    "urgent":"true",
    "distance":"distance",
    "request_type":"request_type",
    "estimated_duration":"estimated_duration"
}
```
#### AcceptRequest
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/AcceptRequest
+ Method: POST
+ Content type: text/plain (HEADER)
+ Authorization: Bearer <ID_TOKEN>
+ Body:

```
{
    "request_id":"request_id",
    "uid":"user_id"
}
```

## Calendar

#### New Event
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/NewEvent
+ Method: POST
+ Content type: text/plain (HEADER)
+ Authorization: Bearer <ID_TOKEN>
+ Body:

```
{
    "uid":"user_id",
    "eventinfo": {
        "uid":"upm_id",
        "date":"date any format",
        "title":"title",
        "lat":"latitude",
        "lon":"longitude",
        "daytime":"(1, 2 o 3 que serien mati, tarda i dia sencer)"
        ... other camps as description, etc.
    }
}
```
+ Returns: 200, 403 (unauthorized)

#### Edit Event
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/EditEvent
+ Method: POST
+ Content type: text/plain (HEADER)
+ Authorization: Bearer <ID_TOKEN>
+ Body:

```
{
    "uid":"user_id",
    "eventid":"event_id",
    "eventinfo": ... 
    (posar localitzacio, data, descripcio, userid (tot i que sigui redundant), participants...)
}
```
+ Returns: 200, 403 (unauthorized)

#### Delete Event
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/DeleteEvent
+ Method: POST
+ Content type: text/plain (HEADER)
+ Authorization: Bearer <ID_TOKEN>
+ Body:

```
{
    "eventid":"event_id",
    "uid":"user_id"
}
```
+ Returns: 200, 403 (unauthorized)


#### Join Event
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/JoinEvent
+ Method: POST
+ Content type: text/plain (HEADER)
+ Authorization: Bearer <ID_TOKEN>
+ Body:

```
{
    "eventid":"event_id",
    "uid":"user_id"
}
```
+ Returns: 200, 403 (unauthorized)


#### Leave Event
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/LeaveEvent
+ Method: POST
+ Content type: text/plain (HEADER)
+ Authorization: Bearer <ID_TOKEN>
+ Body:

```
{
    "eventid":"event_id",
    "uid":"user_id"
}
```
+ Returns: 200, 403 (unauthorized)

#### GetEventPublic
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/GetEventPublic
+ Method: POST
+ Content type: text/plain (HEADER)
+ Body:

```
{
    "eventid":"event_id"
}
```
+ Returns: 200, 403 (unauthorized) + event full info

#### GetEventPrivate
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/GetEventPrivate
+ Method: POST
+ Content type: text/plain (HEADER)
+ Authorization: Bearer <ID_TOKEN>
+ Body:

```
{
    "eventid":"event_id",
    "uid":"user_id"
}
```
+ Returns: 200, 403 (unauthorized) + event full info

#### GetEventsUPM
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/GetEventsUPM
+ Method: POST
+ Content type: text/plain (HEADER)
+ Authorization: Bearer <ID_TOKEN>
+ Body:

```
{
    "uid":"user_id"
}
```
+ Returns: 200, 403 (unauthorized) + array of events (short)

#### GetEventsVolunteer
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/GetEventsVolunteer
+ Method: POST
+ Content type: text/plain (HEADER)
+ Authorization: Bearer <ID_TOKEN>
+ Body:

```
{
    "uid":"user_id"
}
```
+ Returns: 200, 403 (unauthorized) + array of events (short)

#### GetPublicEventsFiltered
+ URL: https://us-central1-pes11-mobilitat.cloudfunctions.net/GetPublicEventsFiltered
+ Method: POST
+ Content type: text/plain (HEADER)
+ Body:

```
{
    "date":"data en mateix format que el new event",
    "daytime":"1, 2 o 3 segons new event",
    "dist":{
        "lat":"latitud",
        "lon":"longitud",
        "radius":"distancia maxima"
    }
}
```
+ Returns: 200, 403 (unauthorized) + array of events (short)
