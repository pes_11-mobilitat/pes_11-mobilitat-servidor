const express = require("express");
const cron = require("node-cron");
const request = require('request');


//create express app
const app = express();

//port at which the server will run
const port = 4000;

//create end point
app.get("/", (request, response) => {
  //send 'Hi, from Node server' to client
  response.send("Hi, from Node server");
});

//start server and listen for the request
app.listen(port, () =>
  //a callback that will be called as soon as server start listening
  onStart()
);

function onStart(){
 console.log(`server is listening at http://localhost:${port}`);
  var task = cron.schedule('* * * * * *', () => {
	  console.log('Printing this line every second in the terminal');
	  sendRequestUpdateDistance();
	  sendRequestTimeExpired();
  },{
	  scheduled: true
  });

  task.start();
}

function sendRequestUpdateDistance(){
  var url = "https://us-central1-pes11-mobilitat.cloudfunctions.net/UpdateDistance";
  request(url, (err, res, body) => {
    if (err) { return console.log(err); }
    console.log(body.url);
    console.log(body.explanation);
  });
  
function sendRequestTimeExpired(){
  var url = "https://us-central1-pes11-mobilitat.cloudfunctions.net/MatchTimeExpired";
  request(url, (err, res, body) => {
    if (err) { return console.log(err); }
    console.log(body.url);
    console.log(body.explanation);
  });
}
